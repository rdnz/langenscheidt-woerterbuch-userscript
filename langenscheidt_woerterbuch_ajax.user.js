// ==UserScript==
// @name langenscheidt_woerterbuch_ajax
// @match file:///Users/*/langenscheidt_woerterbuch.html
// @match file:///Users/*/langenscheidt_woerterbuch.html?*
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @grant GM.xmlHttpRequest
// @grant unsafeWindow
// @connect woerterbuch.langenscheidt.de
// @run-at document-start
// ==/UserScript==

unsafeWindow.ajax_gm_jquery = settings => {
  GM.xmlHttpRequest({
    "method": settings.type,
    "url": settings.url,
    "data": $.param(settings.data),
    "onerror": () => {if ("error" in settings) settings.error();},
    "onload": response => {
      if ("success" in settings)
        settings.success(settings.dataType === "json" ?
          JSON.parse(response.responseText) : response.responseText
        );
    },
    "headers": {"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"}
  });
};
