const ng = angular.element(document).scope().$$childHead;

copy(JSON.stringify(
  ng.active.subject.cards
    .map(card => {
      card.answer_processed =
      /(?:\(.+\) +)?(.+?)(?: +\(.+\))?$/.exec(
        card.content.answer.split(/\n|\[{~| \+/, 1)[0]
      )[1];
      switch (card.answer_processed) {
      case "es regnet":
        card.answer_processed = "regnen";
        break;
      case "danken für":
        card.answer_processed = "danken";
        break;
      case "Dank für":
        card.answer_processed = "Dank";
        break;
      case "am Morgen / morgens":
        card.answer_processed = "morgens";
        break;
      case "am Mittag / mittags":
        card.answer_processed = "mittags";
        break;
      case "ein paar":
        card.answer_processed = "paar";
        break;
      case "es schneit":
        card.answer_processed = "schneien";
        break;
      case "vorstellen, sich":
        card.answer_processed = "vorstellen";
        break;
      case "am Vormittag / vormittags":
        card.answer_processed = "vormittags";
        break;
      case "in der Nacht / nachts":
        card.answer_processed = "nachts";
        break;
      case "am Nachmittag / nachmittags":
        card.answer_processed = "nachmittags";
        break;
      case "ein bisschen":
        card.answer_processed = "bisschen";
        break;
      case "füllen in":
        card.answer_processed = "füllen";
        break;
      case "Postleitzahl, PLZ":
        card.answer_processed = "Postleitzahl";
        break;
      case "Jura / Rechtswissenschaften":
        card.answer_processed = "Jura";
        break;
      case " Kilogramm / Kilo":
        card.answer_processed = "Kilogramm";
        break;
      }
      return card;
    })
    .filter(card =>
      (card.answer_processed.match(/ /g) || []).length == 0 &&
    !/(?:\.|\?)$/.test(card.answer_processed) &&
    !/[^\wäöüÄÖÜß-]/.test(card.answer_processed) ||
    card.answer_processed == "Pommes frites"
    )
    .map(card => {
      switch (card.answer_processed) {
      case "Dank":
        card.answer_processed = "Dank;der";
        break;
      case "Postleitzahl":
        card.answer_processed = "Postleitzahl;die";
        break;
      case "Kilogramm":
        card.answer_processed = "Kilogramm;das";
        break;
      default: {
        const article_match =
      (new RegExp(
        "^\\((der|die|das|der/die) ?\\) " + card.answer_processed + "(?: \\(|\\[{~)"
      )).exec(card.content.answer);
        let article = "undefined";
        if (article_match !== null)
          article = article_match[1];
        card.answer_processed += ";" + article;
        break;
      }
      }
      return card;
    })
    .map(card => card.answer_processed)
))


  .map(card => {
    return {
      "unit": card.unit.content.name,
      "question": card.content.question.split(/\n|\[{~/, 1)[0],
      "answer": card.content.answer.split(/\n|\[{~/, 1)[0],
      "id": card.id
    };
  });




ng.active.subject.cards
  .map(card => {
    card.answer_processed =
      /(?:\(.+\) +)?(.+?)(?: +\(.+\))?$/.exec(
        card.content.answer.split(/\n|\[{~| \+/, 1)[0]
      )[1];
    return card;
  })
  .filter(card =>
    (card.answer_processed.match(/ /g) || []).length == 0 &&
    !/(?:\.|\?)$/.test(card.answer_processed) &&
    !/[^\wäöüÄÖÜß-]/.test(card.answer_processed) ||
    card.answer_processed == "Pommes frites"
  )
  .filter(card =>
    /^\W*(?:der|die|das)/.test(card.content.answer) &&
    !(new RegExp(
      "^\\((?:der|die|das|der/die) ?\\) " + card.answer_processed + "(?: \\(|\\[{~)"
    )).test(card.content.answer)
  )
  .map(card => card.content.answer);

ng.active.subject.cards
  .map(card => {
    card.answer_processed =
      /(?:\(.+\) +)?(.+?)(?: +\(.+\))?$/.exec(
        card.content.answer.split(/\n|\[{~| \+/, 1)[0]
      )[1];
    switch (card.answer_processed) {
    case "es regnet":
      card.answer_processed = "regnen";
      break;
    case "danken für":
      card.answer_processed = "danken";
      break;
    case "Dank für":
      card.answer_processed = "Dank";
      break;
    case "am Morgen / morgens":
      card.answer_processed = "morgens";
      break;
    case "am Mittag / mittags":
      card.answer_processed = "mittags";
      break;
    case "ein paar":
      card.answer_processed = "paar";
      break;
    case "es schneit":
      card.answer_processed = "schneien";
      break;
    case "vorstellen, sich":
      card.answer_processed = "vorstellen";
      break;
    case "am Vormittag / vormittags":
      card.answer_processed = "vormittags";
      break;
    case "in der Nacht / nachts":
      card.answer_processed = "nachts";
      break;
    case "am Nachmittag / nachmittags":
      card.answer_processed = "nachmittags";
      break;
    case "ein bisschen":
      card.answer_processed = "bisschen";
      break;
    case "füllen in":
      card.answer_processed = "füllen";
      break;
    case "Postleitzahl, PLZ":
      card.answer_processed = "Postleitzahl";
      break;
    case "Jura / Rechtswissenschaften":
      card.answer_processed = "Jura";
      break;
    case " Kilogramm / Kilo":
      card.answer_processed = "Kilogramm";
      break;
    }
    return card;
  })
  .filter(card =>
    (card.answer_processed.match(/ /g) || []).length == 0 &&
    !/(?:\.|\?)$/.test(card.answer_processed) &&
    /[^\wäöüÄÖÜß-]/.test(card.answer_processed) ||
    card.answer_processed == "Pommes frites"
  )
  .map(card => card.answer_processed);
