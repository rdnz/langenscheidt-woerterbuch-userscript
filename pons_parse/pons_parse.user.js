// ==UserScript==
// @name pons_parse
// @match file:///Users/rednaz/Desktop/langenscheidt_woerterbuch/pons_parse.html
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @connect de.pons.com
// @grant GM.xmlHttpRequest
// ==/UserScript==

const remove_tags = (html, start_inside = true) => html
  .split(/<|>/)
  .map((e, i) =>
    i%2 == start_inside ?
      e :
      (/^\/?acronym/.test(e) ? "<" + e + ">" : "")
  )
  .join("")
  .replace(/\s+/g, " ")
  .trim();

// checkbox show idioms,
GM.xmlHttpRequest({
  "method": "GET",
  "url": "https://de.pons.com/%C3%BCbersetzung?l=deen&q=" + "leben",
  "onload": response => {
    // sorry, it is disgusting, but about fivefold faster than $(response.responseText)
    // direction de->en, no results-examples-internet
    let lang_pattern =
      / (?:class="results-examples-internet"|id="(de|en)" class="lang[ "])/g;
    let lang_match;
    for (;;) {
      lang_match = lang_pattern.exec(response.responseText);
      if (lang_match === null)
        return;
      if (lang_match[1] == "de")
        break;
    }
    const de_begin_index = lang_match.index;
    for (;;) {
      lang_match = lang_pattern.exec(response.responseText);
      if (
        lang_match === null || // no match
        lang_match[1] === undefined || // results-examples-internet
        lang_match[1] != "de" // wrong direction
      )
        break;
    }
    let lang_substring;
    if (lang_match !== null)
      lang_substring =
        response.responseText.slice(de_begin_index, lang_match.index);
    else
      lang_substring = response.responseText.slice(de_begin_index);
    // iterate over rom
    let rom = lang_substring.split(/ class="rom[ "]/);
    rom.splice(0, 1);
    $("#translation").append(rom.map(rom_current => {
      // Wortart
      let rom_dom = $("<li class=\"dictionary_rdnz level1_rdnz\">");
      const wordclass_match = / class="wordclass[ "](.+?)<\/h2/.exec(rom_current);
      rom_dom.html(wordclass_match !== null ?
        remove_tags(wordclass_match[1]) :
        "missing"
      );
      // iterate of translations
      let translation = rom_current.split(/ class="translations[ "]/);
      translation.splice(0, 1);
      return rom_dom.append($("<ol class=\"dictionary_rdnz\">").append(translation.map(translation_current => {
        let translation_dom = $("<li class=\"dictionary_rdnz level2_rdnz\">");
        // Sinn
        if (!/^[^>]*>[^<]*<h3[^>]*>\s*Wendungen:\s*<\/h3/.test(translation_current)) {
          const sense_match =
            / class="sense"[^>]*>\((.+?)\)<\/span>:\s*<\/h3/.exec(translation_current);
          translation_dom.html(sense_match !== null ?
            sense_match[1] :
            "missing"
          );
        }
        else
          translation_dom.text("Wendungen");
        // iterate over dl
        let dl_dom = $("<ol class=\"dictionary_rdnz\">").appendTo(translation_dom);
        let item_previous;
        let dl = translation_current.split(/ class="source[ "]/);
        dl.splice(0, 1);
        for (let dl_current of dl) {
          let dl_current_dom = $("<tr>");
          const source_match =
            /^[^>]*>\s*<\w+ (?:class="([^"]+)")?([\s\S]+?)<\/div/.exec(dl_current);
          $("<td class=\"dictionary_rdnz\">")
            .html(source_match !== null ? source_match[1] : "missing")
            .appendTo(dl_current_dom);
          $("<td class=\"dictionary_rdnz\">")
            .html(source_match !== null ? remove_tags(source_match[2]) : "missing")
            .appendTo(dl_current_dom);
          const target_match = /<div class="target[ "]([\s\S]+?)<\/div/.exec(dl_current);
          $("<td class=\"dictionary_rdnz\">")
            .html(target_match !== null ? remove_tags(target_match[1]) : "missing")
            .appendTo(dl_current_dom);
          let item_current = $("<li class=\"dictionary_rdnz\">")
            .addClass(source_match[1] + "_rdnz")
            .append($("<table class=\"dictionary_rdnz\">")
              .append($("<tbody>")
                .append(dl_current_dom)
              )
            );
          if (
            (
              source_match[1] == "example" ||
              source_match[1] == "full_collocation"
            ) &&
            item_previous !== undefined
          ) {
            item_current.addClass("level4_rdnz");
            const list_previous = $("ol", item_previous);
            if (list_previous.length != 0)
              item_current.appendTo(list_previous);
            else
              item_current.appendTo($("<ol class=\"dictionary_rdnz\">").appendTo(item_previous));
          }
          else { // "headword", "grammatical_construction", "idiom_proverb"
            item_current.addClass("level3_rdnz");
            item_current.appendTo(dl_dom);
            item_previous = item_current;
          }
        }
        return translation_dom;
      })));
    }));
    $("li.level1_rdnz").filter(":even").addClass("odd_rdnz");
    for (let j = 2; j <= 4; ++j)
      $("li.level" + j + "_rdnz").each((j, element) => {
        const element_jquery = $(element);
        if (element_jquery.index()%2 == element_jquery.parents("li").first().hasClass("odd_rdnz"))
          element_jquery.addClass("odd_rdnz");
      });
  }
});
