// ==UserScript==
// @name langenscheidt_woerterbuch_category
// @match https://woerterbuch.langenscheidt.de/ssc/search.html
// @match https://woerterbuch.langenscheidt.de/ssc/search.html?*
// @match file:///Users/*/langenscheidt_woerterbuch.html
// @match file:///Users/*/langenscheidt_woerterbuch.html?*
// @match https://de.pons.com/%C3%BCbersetzung?*
// @match https://en.pons.com/translate?*
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @require jquery-ui-1.12.1.js
// @grant GM.setValue
// @grant GM.getValue
// @grant GM.listValues
// @grant GM.deleteValue
// ==/UserScript==

"use strict";

const frequented_threshold = 5;
const view_maximum_growth = 12;
const view_slope = 2;
const text = {
  "view": " Aufruf",
  "views": " Aufrufe",
  "library_button": "Bibliothek",
  "library_title": "Bibliothek",
  "visited_tab": "Besucht",
  "active_category": "Aktiv",
  "inactive_category": "Inaktiv",
  "word_column": "Wort",
  "article_column": "Artikel",
  "view_column": "Aufrufe",
  "view_date_column": "erster Aufruf",
  "activate_date_column": "aktiviert",
  "phase6_column": "Phase 6",
  "category_column": "Kategorie",
  "export_date_column": "exportiert",
  "library_ok": "Ok",
  "save_button": "Speichern"
};
/* english, incomplete
const text = {
  "view": " view",
  "views": " views",
  "library_button": "Library",
  "library_title": "Library",
  "visited_tab": "Visited",
  "active_category": "Active",
  "inactive_category": "Inactive",
  "word_column": "Word",
  "article_column": "Article",
  "view_column": "Views",
  "view_date_column": "First Visited",
  "activate_date_column": "Activated",
  "phase6_column": "Phase 6",
  "category_column": "Category",
  "export_date_column": "Exported",
  "save_button": "Ok"
};
*/

// let $,GM,build,phase6_id,phase6_record;
const main = () => {
  // storage_reset();
  if (!/pons\.com$/.test(window.location.hostname)) {
    // save + phase6
    $("head").append(langenscheidt.style_sheet);
    langenscheidt.activation_phase6_view_insert.observe(
      $("#searchResults")[0],
      {childList: true}
    );
    $(window.document).tooltip({
      "items": ".phase6_rdnz, .date_column_rdnz",
      "content": function() {return this.title.replace(/\n/g, "<br>");}
    });
    $("#searchResults").on("click", ".deactivate_rdnz, .activate_rdnz", langenscheidt.activate);

    // library
    langenscheidt.jquery_ui_load();
    $("<button id=\"library\">" + text.library_button + "</button>")
      .button()
      .appendTo("aside.sidebarFilter")
      .on("click", () => {
        if (langenscheidt.library.dom.tabs("option", "active") === 1) // if tab switching is not necessary,
          langenscheidt.library.load_tab(  // execute tab switching tasks manually
            langenscheidt.library.dom.children("div").eq(1)
          );
        langenscheidt.library.dom.tabs("option", "active", 1);
        langenscheidt.library.dom.dialog("open");
      });
    langenscheidt.library.dom
      .tabs({
        "active": 1,
        "beforeActivate": (event, ui) => {
          langenscheidt.library.load_tab(ui.newPanel);
        }
      })
      .dialog({
        "autoOpen": false,
        "width": 1000,
        "title": text.library_title,
        // "draggable": false,
        // "resizable": false,
        "modal": true,
        "buttons": [{
          "text": text.library_ok,
          "click": () => {langenscheidt.library.dom.dialog("close");}
        }]
      });
    $(window.document).on("click", ".ui-widget-overlay", () => {
      langenscheidt.library.dom.dialog("close");
    });
    // $("tbody.table_save_rdnz").selectable();
    $("th.table_save_rdnz").on("click", langenscheidt.library.sort_rows);
    $(langenscheidt.library.dom).on("dblclick", ".category_column_rdnz", langenscheidt.library.activate);
  }
  else {
    const id_match = /[#&]id_rdnz=([^&]+)/.exec(window.location.hash);
    if (id_match !== null) {
      $(document).scrollTop($("div#results-tab-dict").offset().top);
      $("head").append(pons.style_sheet);
      $("head").append(style_sheet_jquery_ui("sunny"));

      // checkboxes
      const translation_all =
        $("div#results-tab-dict, div#results-tab-examples").find(".translations");
      const translation_option =
        translation_all
          .find("dd")
          .find("ul.translation-options");
      translation_option.prepend(
        "<li class=\"select_rdnz\"><input type=\"checkbox\" class=\"select_rdnz\"></li>"
      );

      // save button
      const save_button = $("<button id=\"save_rdnz\">" + text.save_button + "</button>")
        .button()
        .appendTo("#wrap");
      // $(window.document).on("keypress", () => {
      //   console.log($("#save_rdnz").outerWidth(true));
      // });
      const save_button_width = 114; // todo: can we determine this value dynamically on a faster computer?
      save_button.css({"left":
        ($("#wrap").children(".container").first().offset().left-save_button_width)
        + "px"
      });

      // reverse button
      translation_all.find("dt").prepend(() => {
        const label = $(
          "<label class=\"reverse_rdnz\">\
          <img class=\"reverse_rdnz\" src=\"build.data_url_of_file(sync.svg)\" alt=\"reverse\">\
          </label>"
        );
        $("<input type=\"checkbox\" class=\"reverse_rdnz\">")
          .appendTo(label)
          .checkboxradio({"icon": false});
        return label;
      });

      // behavior
      (async () => {
        // The storage object might better be the instance of a proper class
        // incorporating pons.update_storage
        // and calling it automatically on change to the data.
        // An obstacle to this would be
        // that .data("recordrdnz") partly references elements of storage.flash_card.
        // How do I redesign this to avoid that?
        const storage = {};
        storage.id = id_match[1] + ";flashcard";
        storage.flash_card_text = await GM.getValue(storage.id);
        if (storage.flash_card_text === undefined)
          storage.flash_card = [];
        else {
          storage.flash_card = JSON.parse(storage.flash_card_text);
          pons.process_initial_flash_card(storage.flash_card);
        }
        translation_all.find("dl").each((index, translation) => {
          pons.assign_record_to_translation($(translation), storage.flash_card, false);
        });
        translation_all.find("dl").each((index, translation) => {
          pons.assign_record_to_translation($(translation), storage.flash_card, true);
        });
        // checkboxes
        translation_option.find("input.select_rdnz").on("change", event => {
          pons.change_select_rdnz($(event.target), storage);
        });
        // save button
        save_button.on("click", () => {
          pons.check_correct_log(storage);
        });
        // reverse button
        translation_all.find("input.reverse_rdnz").on("change", event => {
          pons.reverse_translation($(event.target).closest("dl"), storage);
        });
      })();
    }
  }
  storage_print();
};
const langenscheidt = {
  "activation_phase6_view_insert": new MutationObserver(() => {
    eval_content_scope("$(\".toggleView\").trigger(\"click\")"); // Why eval_content_scope?
    $("a[id^=\"GW-DaF\"]").each(async (index, link) => {
      const link_jquery = $(link);
      const word = langenscheidt.word_of_link(link);
      const article = langenscheidt.article_of_link(link_jquery);
      const id = word + ";" + article;
      const phase6 = langenscheidt.phase6_of_id(id);

      let vocabulary_item = await GM.getValue(id);
      if (vocabulary_item === undefined)
        vocabulary_item = {
          "view_count": 1,
          "view_date": Date.now(),
          "activate_data": null,
          "word": word,
          "article": article,
          "phase6": phase6
        };
      else {
        vocabulary_item = JSON.parse(vocabulary_item);
        ++vocabulary_item.view_count;
        vocabulary_item.phase6 = phase6; // the phase6 dump is never perfect
      }
      const vocabulary_item_text = JSON.stringify(vocabulary_item);
      GM.setValue(id, vocabulary_item_text);

      // insert
      const search_result = link_jquery.parent();
      // activation
      $("<div>")
        .addClass(
          (vocabulary_item.activate_data === null ? "activate" : "deactivate") +
          "_rdnz"
        )
        .data("idrdnz", id)
        .data("recordrdnz", vocabulary_item_text)
        .insertBefore(search_result);
      // phase 6
      if (phase6 !== null)
        $("<div class=\"phase6_rdnz\">")
          .prop("title",
            phase6.unit + "\n" +
            phase6.question + "\n" +
            phase6.answer
          )
          .insertBefore(search_result);
      // view
      $("<div class=\"view_rdnz\">")
        .addClass("view_rdnz" + vocabulary_item.view_count)
        .addClass(vocabulary_item.view_count >= frequented_threshold ? "view_frequented_rdnz" : "")
        .text(vocabulary_item.view_count + (vocabulary_item.view_count === 1 ? text.view : text.views))
        .css({
          "font-size":
            (
              Math.atan(
                (vocabulary_item.view_count-1) * Math.PI * view_slope / view_maximum_growth / 2
              ) * 2 * view_maximum_growth / Math.PI +
              13
            ) + "px"
        })
        .insertBefore(search_result);
    });
  }),
  "activate": event => {
    const target = $(event.target);
    const id = target.data("idrdnz");
    const vocabulary_item = JSON.parse(target.data("recordrdnz"));
    if (target.hasClass("activate_rdnz"))
      vocabulary_item.activate_data = Date.now();
    else
      vocabulary_item.activate_data = null;
    GM.setValue(id, JSON.stringify(vocabulary_item));
    target
      .toggleClass("activate_rdnz")
      .toggleClass("deactivate_rdnz");
    // activate_date is purposefully not updated in data-recordrdnz
  },
  "word_of_link": link => {
    const word_match = /GW-DaF(.+)/.exec(link.id);
    if (word_match === null)
      return null;
    // todo: ist ae = auml eindeutig?
    return (word_match[1]
      .replace(/auml/g, "ä")
      .replace(/ouml/g, "ö")
      .replace(/uuml/g, "ü")
      .replace(/Auml/g, "Ä")
      .replace(/Ouml/g, "Ö")
      .replace(/Uuml/g, "Ü")
      .replace(/szlig/g, "ß")
      .replace(/hyphen/g, "-")
      .replace(/uc([A-ZÄÖÜ])/g, "$1"));
  },
  "article_of_link": link_jquery => {
    const article_jquery = link_jquery.next("span.elementItalic");
    let article = null;
    if (article_jquery.length !== 0) {
      const article_match = / (der|die|das|der\/die)/.exec(article_jquery.text());
      if (article_match !== null)
        article = article_match[1];
    }
    return article;
  },
  "phase6_of_id": id => {
    const phase6_index =
      langenscheidt.phase6_id.indexOf(id.replace(/-h\d+;/, ";"));
    return (phase6_index !== -1 ?
      langenscheidt.phase6_record[phase6_index] :
      null);
  },
  "jquery_ui_load": () => {
    // clean up unused first-party ui classes
    $("*").each((index, element) => {
      const class_name = element.className.split(/\s+/);
      for (const class_name_current of class_name)
        if (class_name_current.slice(0,3) === "ui-")
          $(element).removeClass(class_name_current);
    });
    const suggestion_list = $("div.searchAutocomplete").children("ul");
    const mutation_suggestion = new MutationObserver(() => {
      suggestion_list.find("li").removeClass("ui-menu-item");
      suggestion_list.find("a").removeClass("ui-corner-all");
    });
    mutation_suggestion.observe(suggestion_list[0], {childList: true});

    // load jquery ui
    $("head").append(style_sheet_jquery_ui("sunny"));
  },
  "library": {
    "activate": async event => {
      const cell = $(event.target);
      const id = cell.parent().data("id");
      let word = JSON.parse(await GM.getValue(id));
      if (word.activate_data === null) {
        word.activate_data = Date.now();
        cell.text(text.active_category);
        cell.parent().find(".activate_date_column_rdnz").text(
          format_timestamp_date(word.activate_data)
        );
      }
      else {
        word.activate_data = null;
        cell.text(text.visited_category);
        cell.parent().find(".activate_date_column_rdnz").text("");
      }
      GM.setValue(id, JSON.stringify(word));
      // todo: page might need updates too
    },
    "dom": $(
      "<div>\
      <ul>\
      <li><a href=\"#visited_tab\">" + text.visited_tab + "</a></li>\
      <li><a href=\"#active_tab\">" + text.active_category + "</a></li>\
      </ul>\
      <div id=\"visited_tab\">\
      <table class=\"table_save_rdnz\"><thead class=\"table_save_rdnz\"><tr>\
      <th class=\"table_save_rdnz\">" + text.word_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.article_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.view_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.view_date_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.activate_date_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.phase6_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.category_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.export_date_column + "</th>\
      </tr></thead><tbody class=\"table_save_rdnz\"></tbody></table>\
      </div>\
      <div id=\"active_tab\">\
      <table class=\"table_save_rdnz\"><thead class=\"table_save_rdnz\"><tr>\
      <th class=\"table_save_rdnz\">" + text.word_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.article_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.view_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.view_date_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.activate_date_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.phase6_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.category_column + "</th>\
      <th class=\"table_save_rdnz\">" + text.export_date_column + "</th>\
      </tr></thead><tbody></tbody></table>\
      </div>\
      </div>"
    ),
    "load_tab": async panel => {
      const table_body = panel.find("tbody").text("");
      const id = await GM.listValues();
      for (const id_current of id) {
        const word = JSON.parse(await GM.getValue(id_current));
        if (panel[0].id === "visited_tab" || word.activate_data !== null)
          $("<tr class=\"table_save_rdnz\">")
            .data("id", id_current)
            // Wort
            .append(
              $("<td class=\"table_save_rdnz\">").append(
                $("<a class=\"openSearchLink word_column_rdnz\">")
                  .data("query", word.word.replace(/-h(\d)+$/, ""))
                  .html(word.word.replace(/-h(\d)+$/, "<sup>$1</sup>"))
              )
            )
            // Artikel
            .append(
              $("<td class=\"table_save_rdnz\">")
                .text(word.article !== null ? word.article : "")
            )
            // Aufrufe
            .append(
              $("<td class=\"table_save_rdnz view_column_rdnz\">")
                .text(word.view_count)
                .data("content", word.view_count)
            )
            // erster Aufruf
            .append(
              $("<td class=\"table_save_rdnz date_column_rdnz\">")
                .text(format_timestamp_date(word.view_date))
                .data("content", word.view_date)
                .prop("title", format_timestamp_time(word.view_date))
            )
            // aktiviert
            .append(
              $("<td class=\"table_save_rdnz date_column_rdnz\">")
                .text(word.activate_data !== null ?
                  format_timestamp_date(word.activate_data) :
                  ""
                )
                .data("content", word.activate_data)
                .prop("title", word.activate_data !== null ?
                  format_timestamp_time(word.activate_data) :
                  ""
                )
            )
            // Phase 6
            .append(
              $("<td class=\"table_save_rdnz\">")
                .text(word.phase6 !== null ?
                  word.phase6.unit + "\n" +
                  word.phase6.question + "\n" +
                  word.phase6.answer :
                  ""
                )
            )
            // Kategorie
            .append(
              $("<td class=\"table_save_rdnz category_column_rdnz\">")
                .text(word.activate_data !== null ? text.active_category : text.inactive_category)
                .data("content", word.activate_data === null ? 0 : 1)
            )
            // exportiert
            .append(
              $("<td class=\"table_save_rdnz\">Neu</td>")
            )
            .appendTo(table_body);
      }
    },
    "sort_rows": event => {
      const target = $(event.target);
      const column_header = target.siblings();
      let direction;
      if (!target.hasClass("ascending")) {
        direction = 1;
        target.addClass("ascending");
        if (!target.hasClass("descending")) {
          column_header.removeClass("ascending");
          column_header.removeClass("descending");
        }
        else
          target.removeClass("descending");
      }
      else {
        direction = -1;
        target.removeClass("ascending");
        target.addClass("descending");
      }
      const column_index = target.index();
      let compare;
      switch (target.text()) {
      // compare text
      case text.word_column:
      case text.article_column:
      case text.phase6_column: {
        compare = (row1, row2) =>
          direction *
            $(row1).children().eq(column_index).text().localeCompare(
              $(row2).children().eq(column_index).text(), "de-DE"
            );
        break;
      }
      // compare numbers
      case text.view_column:
      case text.view_date_column:
      case text.category_column: {
        compare = (row1, row2) =>
          direction * (
            $(row1).children().eq(column_index).data("content")-
              $(row2).children().eq(column_index).data("content")
          );
        break;
      }
      // compare numbers and null
      case text.activate_date_column: {
        compare = (row1, row2) => {
          const content1 = $(row1).children().eq(column_index).data("content");
          const content2 = $(row2).children().eq(column_index).data("content");
          if (content1 !== null)
            if (content2 !== null)
              return direction * (content1-content2);
            else
              return -direction;
          else
          if (content2 !== null)
            return direction;
          else
            return 0;
        };
        break;
      }
      }
      sort_children(target.closest("table").find("tbody"), compare);
    }
  },
  "style_sheet": "<style type=\"text/css\">build.include_into_javascript_string(langenscheidt.css)</style>",
  "phase6_id": build.include_into_javascript(phase6_id.json),
  "phase6_record": build.include_into_javascript(phase6_record.json)
};
const pons = {
  "reverse_translation": (translation, storage) => {
    const translation_record = translation.data("recordrdnz");
    const source_translation_backup = translation_record.source;
    translation_record.source = translation_record.target;
    translation_record.target = source_translation_backup;
    if (translation_record.connected)
      pons.update_storage(storage);
    pons.reverse_translation_dom(translation);
  },
  "reverse_translation_dom": translation => {
    const source = translation.find("dt").find("div.source").first();
    const target = translation.find("dd").find("div.target").first();
    const target_contents = target.contents();
    target.append(source.contents());
    source.append(target_contents);
    const option_source = translation.find("dt").find(".translation-options").first();
    const option_target = translation.find("dd").find(".translation-options").first();
    const audio_source = option_source.children().first();
    const audio_target = option_target.children(".select_rdnz").next();
    audio_source.insertBefore(audio_target);
    audio_target.prependTo(option_source);
  },
  "change_select_rdnz": (checkbox, storage) => {
    const translation = checkbox.closest("dl");
    const translation_record = translation.data("recordrdnz");
    if (checkbox[0].checked) {
      translation_record.connected = true;
      storage.flash_card.push(translation_record);
    }
    else {
      translation_record.connected = false;
      storage.flash_card.splice(
        storage.flash_card.indexOf(
          translation_record
        ),
        1
      );
    }
    pons.update_storage(storage);
  },
  "update_storage": storage => {
    storage.flash_card_text = JSON.stringify(storage.flash_card, ["source", "target"]);
    GM.setValue(storage.id, storage.flash_card_text);
  },
  "assign_record_to_translation": (translation, flash_card, reversed) => {
    const translation_record_dom = !reversed ?
      pons.translation_record(translation)
      : translation.data("recordrdnz");
    if (translation_record_dom.connected)
      return;
    const source = !reversed ? "source" : "target";
    const target = !reversed ? "target" : "source";
    let translation_record_flash_card = flash_card.find(
      translation_flash_card_current =>
        translation_flash_card_current.source === translation_record_dom[source]
        && translation_flash_card_current.target === translation_record_dom[target]
        && !translation_flash_card_current.connected
    );
    if (translation_record_flash_card !== undefined) {
      if (reversed) {
        const reverse_button = translation.find("input.reverse_rdnz");
        reverse_button[0].checked = true;
        reverse_button.checkboxradio("refresh");
        pons.reverse_translation_dom(translation);
      }
      translation.find("input.select_rdnz")[0].checked = true;
      translation.data("recordrdnz", translation_record_flash_card);
      translation_record_flash_card.connected = true;
    }
    else if (!reversed)
      translation.data("recordrdnz", translation_record_dom);
  },
  "translation_record": translation => {
    const source = translation.find("dt").find("div.source").first();
    const target = translation.find("dd").find("div.target").first();
    source.add(target).find("acronym").each((index, acronym) => {
      const acronym_jquery = $(acronym);
      acronym_jquery.attr("data-italicrdnz", acronym_jquery.css("font-style") === "italic" ? "true" : "false");
    });
    const source_clone = source.clone();
    const target_clone = target.clone();
    source_clone.add(target_clone).find("acronym").replaceWith(function() {
      let replacement = this.title;
      if (
        $(this).data("italicrdnz")
        && replacement.length !== 0
        && (replacement[0] !== "(" || replacement[replacement.length-1] !== ")")
      )
        replacement = "(" + replacement + ")";
      return replacement; // have forgone span tags, hope that that is consistent with the specification of replaceWith
    });
    return ({
      "source": source_clone.text().trim().replace(/\s+/g, " "),
      "target": target_clone.text().trim().replace(/\s+/g, " "),
      "connected": false
    });
  },
  "process_initial_flash_card": flash_card => {
    for (const translation_record of flash_card)
      translation_record.connected = false;
  },
  "check_correct_log": async storage => {
    const flash_card_text_actual = await GM.getValue(storage.id);
    if (flash_card_text_actual !== storage.flash_card_text) {
      GM.setValue(storage.id, storage.flash_card_text);
      const id_log = storage.id + ";log";
      const log_text = await GM.getValue(id_log);
      let log;
      if (log_text === undefined)
        log = [];
      else
        log = JSON.parse(log_text);
      log.push({
        "time": Date.now(),
        "expected": storage.flash_card_text,
        "actual": flash_card_text_actual
      });
      GM.setValue(id_log, JSON.stringify(log));
    }
  },
  "style_sheet": "<style type=\"text/css\">build.include_into_javascript_string(pons.css)</style>"
};
const style_sheet_jquery_ui = theme =>
  "<link rel=\"stylesheet\" href=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/"
  + theme
  + "/jquery-ui.css\">";
const format_timestamp_date =
  timestamp => (new Date(timestamp)).toLocaleDateString("de-DE");
const format_timestamp_time =
  timestamp => (new Date(timestamp)).toLocaleDateString(
    "de-DE",
    {"weekday": "long", "hour": "numeric", "minute": "numeric", "second": "numeric"}
  );
const storage_print = () => {
  // $(window.document).on("keydown", event => {
  //   if (event.which === 76) // l
  (async () => {
    const key = await GM.listValues();
    for (const key_current of key)
      console.log(key_current + "\n" + await GM.getValue(key_current) + "\n\n");
  })();
  // });
};
const storage_reset = async () => {
  const key = await GM.listValues();
  for (const key_current of key)
    GM.deleteValue(key_current);
};
const sort_children = (parent, compare) => {
  const child = parent.children().toArray().sort(compare);
  for (const child_current of child)
    parent.append(child_current);
};
const eval_content_scope = script => {
  $("<script>" + script + "</script>").appendTo("body").remove();
};
main();
