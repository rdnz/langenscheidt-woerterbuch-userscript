// ==UserScript==
// @name langenscheidt_woerterbuch_navigate
// @match https://woerterbuch.langenscheidt.de/ssc/search.html
// @match https://woerterbuch.langenscheidt.de/ssc/search.html?*
// @match file:///Users/*/langenscheidt_woerterbuch.html
// @match file:///Users/*/langenscheidt_woerterbuch.html?*
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @grant GM.openInTab
// ==/UserScript==

// modifier key timeout necessary in chrome
// make searchengine

"use strict";

const eval_content_scope = script => {
  $("<script>" + script + "</script>").appendTo("body").remove();
};

eval_content_scope(
  "$(document).off(\"click\", \".openSearchLink\");$(\"#btn_search\").off(\"click\");$(document).off(\"keypress\")"
);

let open_in_background = false;
let alt_pressed = true;
const url_self =
  window.location.protocol + "//" +
  window.location.host +
  window.location.pathname +
  "?query=";
const search = function(search_query, new_tab_default = true) {
  const url_new = url_self + search_query;
  if (alt_pressed === new_tab_default)
    GM.openInTab(url_new, open_in_background);
  else {
    window.location.href = url_new;
    // $("#fld_query").val(search_query);
    // eval_content_scope(
    //   "getSearchResult(getCores(),\"" +
    //   search_query +
    //   "\",0,DEFAULT_DOCUMENTSPERPAGE)");
  }
};
$(document).on("click", ".openSearchLink", event => {
  search($(event.target).data("query"));
});
$("#fld_query").on("keydown", event => {
  if (event.which === 13)
    search($("#fld_query").val(), false);
});
$("#btn_search").on("click", () => {
  search($("#fld_query").val(), false);
});
$("#searchResults").on("click", "a[href^=\"?query=\"]", event =>{
  event.preventDefault();
  const search_query_match = /\?query= *(.+)/.exec(event.target.href);
  if (search_query_match === null)
    return;
  search(search_query_match[1]);
});
$(document).on("keydown", event => {
  switch (event.which) {
  case 16: // shift
    open_in_background = true;
    break;
  case 18: // alt
    alt_pressed = false;
    break;
  }
});
$(document).on("keyup", event => {
  switch (event.which) {
  case 16: // shift
    open_in_background = false;
    break;
  case 18: // alt
    alt_pressed = true;
    break;
  }
});
