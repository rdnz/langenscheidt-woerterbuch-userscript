// ==UserScript==
// @name timeout_block
// @match *://*.xing.com/*
// @run-at document-start
// @version 1
// ==/UserScript==

const timeout_block_script = document.createElement("script");
timeout_block_script.textContent = "window.setTimeout=()=>{}";
const head_dom = document.getElementsByTagName("head")[0];
head_dom.insertBefore(timeout_block_script, head_dom.firstChild);
