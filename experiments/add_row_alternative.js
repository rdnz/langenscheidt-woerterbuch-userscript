$("tbody", panel).append("\
<tr class=\"table_rdnz\" data-id=\"" + id_current + "\">" +
// Wort
"<td class=\"table_rdnz\">\
<a class=\"openSearchLink\" data-query=\"" + word.word.replace(/-h(\d)+$/, "") + "\">" +
word.word.replace(/-h(\d)+$/, "<sup>$1</sup>") +
"</a>\
</td>" +
// Artikel
"<td class=\"table_rdnz\">" +
(word.article !== undefined ? word.article : "") +
"</td>" +
// Aufrufe
"<td class=\"table_rdnz view_column_rdnz\">" + word.view_count + "</td>" +
// erster Aufruf
"<td class=\"table_rdnz\">" + format_timestamp(word.view_date) + "</td>" +
// aktiviert
"<td class=\"table_rdnz activate_date_column_rdnz\">" +
(word.activate_data !== undefined ?
format_timestamp(word.activate_data) :
""
) +
"</td>" +
// Phase 6
"<td class=\"table_rdnz\">" + (word.phase6 !== undefined ?
word.phase6.unit + "<br>" +
word.phase6.question + "<br>" +
word.phase6.answer :
""
) +
"</td>" +
// Kategorie
"<td class=\"table_rdnz category_column_rdnz\">" + (word.activate_data !== undefined ? text.activated_category : text.visited_category) + "</td>" +
// exportiert
"<td class=\"table_rdnz\">Neu</td>\
</tr>");
