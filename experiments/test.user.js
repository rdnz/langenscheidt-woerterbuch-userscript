// ==UserScript==
// @name test
// @match file:///Users/*/test.html
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @grant GM.setValue
// @grant GM.getValue
// @grant GM.listValues
// @grant GM.deleteValue
// ==/UserScript==

// GM.setValue("0", "0");
let current;
(async () => {
  console.log(current = Number.parseInt(await GM.getValue("0")));
})();

$(window).on("beforeunload", () => {GM.setValue("0", current + 1);});
// window.addEventListener('beforeunload', () => GM.setValue("0", current + 1));
