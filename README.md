# Langenscheidt Woerterbuch Userscript

Userscript for the [Langenscheidt Online-Woerterbuch](http://woerterbuch.langenscheidt.com).

- `langenscheidt_woerterbuch_navigate.user.js` alters the url with every lookup making the lookup history accessible through the web browser's history.
- `langenscheidt_woerterbuch_ajax.user.js` allows a faster use of the website through the local copy `langenscheidt_woerterbuch.html` by providing ajax requests to the langenscheidt servers circumventing the same-origin policy.
- `langenscheidt_woerterbuch_category.user.js`
  - saves visited words
  - provides an option to activate words saving them in another cateogory
  - lists words in those two categories
  - displays a view count for visited words
  - informs if and where the visited word is in [this phase 6 database](https://www.phase-6.de/classic/lerninhalte/Ernst-Klett-Sprachen/Deutsch-DaF/DaF-kompakt-neu/DaF-kompakt-neu-A1-EN.html)
  - allows to select a translation from pons