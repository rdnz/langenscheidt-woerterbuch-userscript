import os, sys, re
from base64 import b64encode

resource_path = "resources"
output_path = "/Library/WebServer/Documents"

media_type = {
  "svg": "image/svg+xml",
  "ico": "image/x-icon"
}

with open(sys.argv[1], "r") as user_script_file:
  user_script = user_script_file.read()

def include_into_javascript_string(match):
  with open(os.path.join(resource_path, match.group(1)), "r") as resource:
    return (resource
      .read()
      .replace("\\", r"\\")
      .replace("\"", r"\"")
      .replace("'", r"\'")
      .replace("\n", " ")
    )
user_script = re.sub(
  r"build.include_into_javascript_string\((.+?)\)",
  include_into_javascript_string,
  user_script
)

def include_into_javascript(match):
  with open(os.path.join(resource_path, match.group(1)), "r") as resource:
    return resource.read()
user_script = re.sub(
  r"build.include_into_javascript\((.+?)\)",
  include_into_javascript,
  user_script
)

def data_url_of_file(file_name):
  data_url_suffix = ".dataurl"
  try:
    with open(os.path.join(resource_path, file_name + data_url_suffix), "r") as file:
      return file.read().replace("\n", "")
  except FileNotFoundError:
    with open(os.path.join(resource_path, file_name), "rb") as file:
      return (
        "data:"
        + media_type[file_name.rsplit(".", 1)[1]]
        + ";base64,"
        + b64encode(file.read()).decode()
      )
user_script = re.sub(
  r"build.data_url_of_file\((.+?)\)",
  lambda match: data_url_of_file(match.group(1)),
  user_script
)
user_script = re.sub(
  r"""(url\(\\["'])((?!data:).+?)(\\["']\))""",
  lambda match: match.group(1) + data_url_of_file(match.group(2)) + match.group(3),
  user_script
)

with open(os.path.join(output_path, sys.argv[1]), "w") as output:
  output.write(user_script)
